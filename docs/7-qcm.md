# QCM

Quelle est la réponse à la question universelle ?

{{ qcm(["$6\\times 7$", "$\\int_0^{42} 1 dx$", "`#!python sum([i for i in range(10)])`", "La réponse D"], [1,2], shuffle = True) }}
