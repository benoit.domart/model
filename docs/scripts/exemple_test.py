assert moyenne2([]) == 0
assert moyenne2([1,2]) == 1.5
assert moyenne2([1]) == 1
assert moyenne2([1,2,3]) == 2
assert moyenne2([1,2,3,4]) == 2.5