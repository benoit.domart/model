# Circuits logiques

[https://logic.modulo-info.ch/](https://logic.modulo-info.ch/){:target="_blank"}

[http://jp.pellet.name/hep/didapro9/](http://jp.pellet.name/hep/didapro9/){:target="_blank"}

<logic-editor></logic-editor>

## Avec un Web Component (mode admin)

???info

    Il y a d'autres modes (Cf. premier lien).

<div style="width: 100%; height: 250px">
  <logic-editor mode="admin">
    <script type="application/json">
      {
        "v": 3,
        "in": [{"pos": [90, 50], "id": 0, "val": 1}, {"pos": [90, 200], "id": 4, "val": 1}],
        "gates": [{"type": "OR", "pos": [210, 110], "in": [1, 2], "out": 3}],
        "out": [{"pos": [380, 150], "id": 5}],
        "wires": [[0, 1], [4, 2], [3, 5]]
      }
    </script>
  </logic-editor>
</div>

## Avec un Web Component (mode test - tryout)

<div style="width: 100%; height: 250px">
  <logic-editor mode="tryout">
    <script type="application/json">
      {
        "v": 3,
        "in": [{"pos": [90, 50], "id": 0, "val": 1}, {"pos": [90, 200], "id": 4, "val": 1}],
        "gates": [{"type": "OR", "pos": [210, 110], "in": [1, 2], "out": 3}],
        "out": [{"pos": [380, 150], "id": 5}],
        "wires": [[0, 1], [4, 2], [3, 5]]
      }
    </script>
  </logic-editor>
</div>

## Avec Markdown

```{logic}
:height: 1130
:mode: tryout

{
  "v": 3,
  "in": [{"pos": [350, 280], "id": 0, "val": 1}, {"pos": [110, 850], "id": 4, "val": 1}],
  "gates": [{"type": "OR", "pos": [430, 300], "in": [1, 2], "out": 3}],
  "out": [{"pos": [1330, 430], "id": 5}],
  "wires": [[0, 1], [4, 2], [3, 5]]
}
```

## Avec une URL

https://logic.modulo-info.ch/?mode=tryout&data=N4KABGBEBukFxgMwBpxQJYDt5gNrEgAcB7AZx1wE4AGZMAVmoF07J0ATHWqaAQwBscARgC+dAiXIIq3AEzVmrDjgAsrPoISiWaSAHNeAFwCmUvAUMBPQsZyQA8gCVIrSRVlDuQzywzZpQnSyvpDEAK6GOIgiOhChERQSZBSIABxejCHKCPQxqHEA7ugATqYUuF6+uGpgwXS4KAxMTCAiQA

## Avec une iframe

<iframe style="width: 100%; height: 250px; border: 0" src="https://logic.modulo-info.ch/?mode=tryout&data=N4KABGBEBukFxgMwBpxQJYDt5gNrEgAcB7AZx1wE4AGZMAVmoF07J0ATHWqaAQwBscARgC+dAiXIIq3AEzVmrDjgAsrPoISiWaSAHNeAFwCmUvAUMBPQsZyQA8gCVIrSRVlDuQzywzZpQnSyvpDEAK6GOIgiOhChERQSZBSIABxejCHKCPQxqHEA7ugATqYUuF6+uGpgwXS4KAxMTCAiQA"></iframe>