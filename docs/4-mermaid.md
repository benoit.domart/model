# Mermaid

On peut directement inclure un diagramme 

???+ note "Entrée"
    ````markdown
    Un _joli_ diagramme

    ```mermaid
    graph TD;
        A-->B;
        A-->C;
        B-->D;
        C-->D;
    ```
    ````

???+ done "Rendu"
    Un _joli_ diagramme

    ```mermaid
    graph TD;
        A-->B;
        A-->C;
        B-->D;
        C-->D;
    ```


Pour de plus amples informations, on pourra consulter le site officiel : <[Mermaid](https://mermaid-js.github.io/mermaid/#/)>

```mermaid
classDiagram
	Notes
	class Notes{
		-notesList: List~Double~
		+Notes()
		+getNotesList() List~Double~
		+addNote(note: Double)
		+computeMean() Double
	}
```

???+exercice "Dans un exercice"
    Ca donne ça :
    
    <center>
        ```mermaid
        classDiagram
            Notes
            class Notes{
                -notesList: List~Double~
                +Notes()
                +getNotesList() List~Double~
                +addNote(note: Double)
                +computeMean() Double
            }
        ```
    </center>

???+video "Cours - C'est quoi un vecteur ?"

	<center>
		[![texte](http://img.youtube.com/vi/W0dfX4sDq5g/0.jpg)](https://www.youtube.com/watch?v=W0dfX4sDq5g "C'est quoi un vecteur"){:target="_blank"}
	</center>
    
???+video
    
    ![type:video](https://www.youtube.com/embed/W0dfX4sDq5g)